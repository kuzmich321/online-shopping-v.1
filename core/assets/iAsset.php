<?php

namespace Core\Assets;

interface iAsset
{
    public function includeJs(): string;

    public function includeCss(): string;
}
