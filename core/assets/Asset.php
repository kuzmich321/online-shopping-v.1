<?php

namespace Core\Assets;

use Core\Assets\iAsset;

abstract class Asset implements iAsset
{
    protected string $rootAssets = '';

    protected string $jsFolder = '/js';
    protected string $cssFolder = '/css';

    public array $js = [];

    public array $css = [];

    public function includeJs(): string
    {
        $jsHTML = '';
        $cdns = $this->js['cdn'] ?? null;

        if ($cdns && is_array($cdns)) {
            foreach ($cdns as $cdn) {
                $jsHTML .= '<script type="text/javascript" src="' . $cdn . '"></script>' . PHP_EOL;
            }
            unset($this->js['cdn']);
        }

        foreach ($this->js as $js) {
            $jsHTML .= '<script type="text/javascript" src="' . $this->rootAssets . $this->jsFolder . DS . $js . '"></script>' . PHP_EOL;
        }

        return $jsHTML;
    }

    public function includeCss(): string
    {
        $cssHTML = '';
        $cdns = $this->css['cdn'] ?? null;

        if ($cdns && is_array($cdns)) {
            foreach ($cdns as $cdn) {
                $cssHTML .= '<link rel="stylesheet" type="text/css" href="' . $cdn . '">' . PHP_EOL;
            }
            unset($this->css['cdn']);
        }

        foreach ($this->css as $css) {
            $cssHTML .= '<link rel="stylesheet" type="text/css" href="' . $this->rootAssets . $this->cssFolder . DS . $css . '">' . PHP_EOL;
        }

        return $cssHTML;
    }
}
