<?php

namespace Core;

use Bootstrap\App;
use Exception;

class View
{
    protected $siteTitle;

    protected $layout;

    protected $content = [];

    protected $currentBuffer;

    private $app;

    public function __construct()
    {
        $this->siteTitle = getenv('DEFAULT_SITE_TITLE');
        $this->layout = getenv('DEFAULT_LAYOUT');
        $this->app = App::getInstance();
    }

    public function render(string $viewName, array $params = [])
    {
        $viewPath = $this->app->getAlias('@views') . DS . $viewName . '.php';

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $this->{$key} = $value;
            }
        }

        try {
            if (!file_exists($viewPath)) {
                throw new Exception("The view {$viewName} does not exist");
            }

            include($viewPath);
            include($this->app->getAlias('@views') . DS . 'layouts' . DS . $this->layout . '.php');
        } catch (Exception $e) {
            user_error($e->getMessage());
        }
    }

    public function component(string $componentName, array $params = [])
    {
        $componentPath = $this->app->getAlias('@views') . DS . 'components' . DS . $componentName . '.php';

        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $this->{$key} = $value;
            }
        }

        try {
            if (!file_exists($componentPath)) {
                throw new Exception("The component {$componentName} does not exist");
            }

            include ($componentPath);
        } catch (Exception $e) {
            user_error($e->getMessage());
        }
    }

    public function content($type)
    {
        return array_key_exists($type, $this->content) ? $this->content[$type] : false;
    }

    public function start($type)
    {
        if (empty($type)) die('You must define a type');
        $this->currentBuffer = $type;
        ob_start();
    }

    public function end()
    {
        if (empty($this->currentBuffer)) die('You must first run the start method');
        $this->content[$this->currentBuffer] = ob_get_clean();
        $this->currentBuffer = null;
    }

    public function getSiteTitle()
    {
        return $this->siteTitle;
    }

    public function setSiteTitle($title)
    {
        $this->siteTitle = $title;
    }

    public function setLayout($path)
    {
        $this->layout = $path;
    }
}