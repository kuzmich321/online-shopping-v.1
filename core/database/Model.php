<?php

namespace Core\Database;

use Core\Database\Database;

class Model
{
    protected static $db;

    protected static $table;

    protected $modelName;

    public function __construct()
    {
        $this->modelName = str_replace(' ', '', ucwords(str_replace('_', ' ', static::$table)));
    }

    public static function getDb(): Database
    {
        if (!self::$db) {
            self::$db = Database::getInstance();
        }
        return self::$db;
    }

    public static function query(string $sql, array $bind = []): Database
    {
        return static::getDb()->query($sql, $bind);
    }

    public function insert(array $fields = [])
    {
        if (empty($fields)) return false;
        if ($this->assign($fields)) {
            static::getDb()->insert(static::$table, $fields);
        }

        return $this;
    }

    public static function update(array $fields = [], $id)
    {
        return empty($fields) ? false : static::getDb()->update(static::$table, $fields, $id);
    }

    public static function delete($id)
    {
        return static::getDb()->delete(static::$table, $id);
    }

    // The method will check if a module has attributes and then it will make it clean if true
    public function assign($params): bool
    {
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                if (property_exists($this, $key)) {
                    $this->{$key} = sanitize($value);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function getCount()
    {
        return static::getDb()->getCount();
    }

    public static function findById($id)
    {
        return static::getDb()->query('SELECT * FROM ' . static::$table . ' WHERE id = ? LIMIT 1;', [$id])->first();
    }

    public static function with(string $tableName, array $assocArrayForJoining = [], string $joinType = '')
    {
        return static::getDb()->join(static::$table, $tableName, $assocArrayForJoining, $joinType);
    }

    public static function paginate(int $perPage = 20, int $offset)
    {
        return static::getDb()->paginate(static::$table, $perPage, $offset);
    }

    public static function getTotalCount()
    {
        return static::getDb()->getTotalCount(static::$table);
    }
}