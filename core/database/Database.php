<?php

namespace Core\Database;

use \PDO;
use \Exception;
use Core\Database\Connection;
use Core\Paginator;

class Database
{
    private const INNER_JOIN = 'INNER JOIN';

    private const RIGHT_JOIN = 'RIGHT JOIN';

    private const LEFT_JOIN = 'LEFT JOIN';

    private const RIGHT_OUTER_JOIN = 'RIGHT OUTER JOIN';

    private const LEFT_OUTER_JOIN = 'LEFT OUTER JOIN';

    private const FULL_JOIN = 'FULL JOIN';

    private const FULL_OUTER_JOIN = 'FULL OUTER JOIN';

    private static $instance = null;

    private $pdo;

    private $query;

    private $error = false;

    private $count = 0;

    private $result;

    private $fetchStyle = PDO::FETCH_OBJ;

    private function __construct(Connection $connection)
    {
        $this->pdo = $connection->pdo;
    }

    public static function connection(string $connectionName): Database
    {
        if ($connectionName) {
            try {
                $config = require_once "config/database.php";

                if (!array_key_exists($connectionName, $config)) {
                    throw new Exception("There is no {$connectionName} connection.");
                }

                $connection = $config[$connectionName];
                return new self(new Connection($connection['driver'], $connection['host'], $connection['database'], $connection['username'], $connection['password']));
            } catch (Exception $err) {
                user_error($err->getMessage());
            }
        }
    }

    public static function getInstance(): Database
    {
        if (!isset(self::$instance)) {
            self::$instance = new self(
                new Connection(getenv('DB_DRIVER'), getenv('DB_HOST'), getenv('DB_NAME'), getenv('DB_USERNAME'), getenv('DB_PASSWORD'))
            );
        }

        return self::$instance;
    }

    public function query(string $sql, array $params = [], $class = false): Database
    {
        $this->error = false;

        if ($this->query = $this->pdo->prepare($sql)) {
            $x = 1;

            if (count($params)) {
                foreach ($params as $param) {
                    $this->query->bindValue($x, $param);
                    $x++;
                }
            }

            if ($this->query->execute()) {
                $this->result = ($class && $this->fetchStyle === PDO::FETCH_CLASS) ?
                    $this->query->fetchAll($this->fetchStyle, $class) : $this->query->fetchAll($this->fetchStyle);

                $this->count = $this->query->rowCount();
            } else {
                $this->error = true;
            }
        }

        return $this;
    }

    public function insert(string $table, array $fields = [])
    {
        $fieldString = '';
        $valueString = '';
        $values = [];

        foreach ($fields as $field => $value) {
            $fieldString .= "{$field},";
            $valueString .= "?,";
            array_push($values, $value);
        }

        $fieldString = trim($fieldString, ',');
        $valueString = trim($valueString, ',');

        $sql = "INSERT INTO {$table} ({$fieldString}) VALUES ({$valueString});";

        return $this->query($sql, $values) ?? false;
    }

    public function update(string $table, array $params = [], $id)
    {
        $updateString = '';
        $values = [];

        foreach ($params as $field => $value) {
            $updateString .= "{$field} = ?,";
            array_push($values, $value);
        }

        array_push($values, $id);

        $updateString = trim($updateString, ',');

        $sql = "UPDATE {$table} SET {$updateString} WHERE id = ?";

        return $this->query($sql, $values) ?? false;
    }

    public function delete(string $table, $id)
    {
        return $this->query("DELETE FROM {$table} WHERE id = ?", [$id]) ?? false;
    }

    public function join(string $table, string $tableToJoin, array $assocArrayForJoining = [], string $joinType = '')
    {
        $joinString = '';

        if (!empty($joinType)) {
            foreach ($this->getJoinTypes() as $join) {
                if ($join === $joinType) {
                    $joinType = $join;
                }
            }
        } else {
            $joinType = self::FULL_JOIN;
        }

        if (!empty($assocArrayForJoining)) {
            foreach ($assocArrayForJoining as $key => $value) {
                $joinString .= "{$table}.{$key} = {$tableToJoin}.{$value},";
            }

            $joinString = trim($joinString, ',');
        } else {
            $joinString .= "{$table}.id = {$tableToJoin}.id";
        }

        $sql = "SELECT * FROM {$table} {$joinType} {$tableToJoin} ON {$joinString}";

        return $this->query($sql) ?? false;
    }

    public function results()
    {
        return $this->result;
    }

    public function first()
    {
        return $this->result[0] ?? [];
    }

    public function getCount()
    {
        return $this->count;
    }

    public function paginate(string $table, int $perPage = 20, int $offset)
    {
        return $this->query("SELECT * FROM {$table} LIMIT {$perPage} OFFSET {$offset}")->results();
    }

    public function getTotalCount(string $table)
    {
        return $this->query("SELECT COUNT(*) AS total FROM {$table}")->first()->total;
    }

    private function getJoinTypes()
    {
        return [
            self::INNER_JOIN,
            self::RIGHT_JOIN,
            self::LEFT_JOIN,
            self::LEFT_OUTER_JOIN,
            self::RIGHT_OUTER_JOIN,
            self::FULL_JOIN,
            self::FULL_OUTER_JOIN
        ];
    }
}
