<?php

namespace Core\Database;

use \PDO;
use \PDOException;

class Connection
{
    public $pdo;

    public function __construct(string $driver = '', string $host = '', string $dbName = '', string $username = '', string $password = '')
    {
        try {
            $this->pdo = new PDO("{$driver}:host={$host};dbname={$dbName}", $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $err) {
            die($err->getMessage());
        }
    }
}
