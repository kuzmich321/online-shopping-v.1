<?php

namespace Core;

use Core\Input;
use Core\View;

class Paginator
{
    public $currentPage;

    public $perPage;

    public $totalCount;

    public $totalPages;

    public $offset;

    public function __construct(int $totalCount, int $perPage = 20)
    {
        $this->setCurrentPage();
        $this->totalCount = $totalCount;
        $this->perPage = $perPage;
        $this->setTotalPages();
        $this->setOffset();
    }

    public function setCurrentPage(): Paginator
    {
        $request = new Input();

        $this->currentPage = !$request->get('page') ? 1 : $request->get('page');

        return $this;
    }

    public function setTotalPages()
    {
        return $this->totalPages = ceil($this->totalCount / $this->perPage);
    }

    public function setOffset(): int
    {
        return $this->offset = ($this->getCurrentPage() - 1) * $this->perPage;
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getTotalPages()
    {
        return $this->totalPages;
    }

    public function getLinkRoute(int $page): string
    {
        $queryString = '';

        if (queryStringExists()) {
            parse_str(getQueryString(), $queryArray);
            if (isset($queryArray['page'])) unset($queryArray['page']);

            if (!empty($queryArray)) {
                $queryString .= '&' . urldecode(http_build_query($queryArray));
            }
        }

        return getRequestPath() . "?page={$page}" . $queryString;
    }
}