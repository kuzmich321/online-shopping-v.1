<?php

namespace Core;

use Core\Contracts\Storage;

class Session implements Storage
{
    public static function set(string $name = '', $value = null): bool
    {
        return ($_SESSION[$name] = $value) ?? false;
    }

    public static function get(string $name = '')
    {
        return $_SESSION[$name] ?? null;
    }

    public static function delete(string $name = ''): bool
    {
        if (self::get($name)) {
            unset($_SESSION[$name]);

            return true;
        }

        return false;
    }
}
