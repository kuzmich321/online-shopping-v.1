<?php

namespace Core;

class Application
{
    private $debug;

    private $logPath;

    protected function __construct()
    {
        $this->setLogPath();
        $this->setDebugMode();
        $this->setErrorReporting();
    }

    private function setDebugMode(): void
    {
        $this->debug = getenv('DEBUG_MODE');
    }

    private function setErrorReporting(): void
    {
        if ($this->isDebugMode()) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
            ini_set('log_errors', true);
            ini_set('error_log', $this->logPath);
        } else {
            error_reporting(0);
            ini_set('display_errors', 0);
        }
    }

    private function setLogPath(): void
    {
        $path = implode(DS, explode('/', getenv('ERRORS_LOG_PATH')));

        $this->logPath = $path;
    }

    private function isDebugMode(): bool
    {
        return $this->debug;
    }
}