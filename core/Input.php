<?php

namespace Core;

class Input
{
    public function isPost(): bool
    {
        return $this->getRequestMethod() === 'POST';
    }

    public function isPut(): bool
    {
        return $this->getRequestMethod() === 'PUT';
    }

    public function isGet(): bool
    {
        return $this->getRequestMethod() === 'GET';
    }

    public function getRequestMethod(): string
    {
        return strtoupper($_SERVER['REQUEST_METHOD']);
    }

    public function get(string $input)
    {
        if (array_key_exists($input, $_REQUEST)) {
            if (is_array($_REQUEST[$input])) {
                $data = [];

                foreach ($_REQUEST[$input] as $field => $value) {
                    $data[$field] = trim(sanitize($value));
                }

                return $data;
            } else {
                return trim(sanitize($_REQUEST[$input]));
            }
        }

        return '';
    }
}
