<?php

namespace Core;

use Core\Session;
use Core\Cookie;
use App\Models\Users;

class AuthorizationFactory
{
    public static $currentUser;

    public static int $role;

    public static bool $loggedIn = false;

    public static function authorize(string $cookieAndSessionName = 'frontend_user_id')
    {
        if (Session::get($cookieAndSessionName)) {
            self::setRole($cookieAndSessionName);
        } elseif (Cookie::get($cookieAndSessionName)) {
            Session::set($cookieAndSessionName, Cookie::get($cookieAndSessionName));
            self::setRole($cookieAndSessionName);
        } else {
            return self::$loggedIn = false;
        }

        return self::$loggedIn = true;
    }

    // By default user id is a number, once it gets overwritten by cookie it becomes a string
    private static function setCurrentUser(string $name)
    {
        $user = is_string(Session::get($name)) ?
            Users::query("SELECT * FROM users WHERE MD5(id::text) = ?", [Session::get($name)])->first() :
            Users::findById(Session::get($name));

        return self::$currentUser = $user;
    }

    private static function setRole(string $name)
    {
        self::setCurrentUser($name);
        self::$role = self::$currentUser->role;
    }

    public static function verifyRole(int $role)
    {
        return self::$role === $role ? true : false;
    }

    public static function isLoggedIn()
    {
        return self::$loggedIn;
    }

    public static function getCurrentUser()
    {
        return self::$currentUser ?? null;
    }
}