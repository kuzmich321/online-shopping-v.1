<?php

namespace Core;

use Core\Contracts\Storage;

class Cookie implements Storage
{
    public static function set(string $name = '', $value = null, int $duration = 0, string $path = ''): bool
    {
        return setcookie($name, $value, time() + $duration, $path) ?? false;
    }

    public static function get(string $name = '')
    {
        return $_COOKIE[$name] ?? null;
    }

    public static function delete(string $name = '', string $path = ''): bool
    {
        return self::set($name, null, -1, $path);
    }
}
