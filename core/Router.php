<?php

namespace Core;

class Router
{
    public static function route($url, $params)
    {
        if (is_dir(ROOT . DS . 'app' . DS . 'controllers' . DS . $url[0])) $dir = array_shift($url);

        $controller = isset($url[0]) && $url[0] !== '' ? ucwords($url[0]) . 'Controller' : getenv('DEFAULT_CONTROLLER');
        $controllerName = $controller;
        array_shift($url);

        $action = $url[0] ?? 'index';
        array_shift($url);

        $url['query'] = $params;

        if ($dir) {
            $controller = 'App\Controllers\\' . ucfirst($dir) . '\\' . $controller;
        } else {
            $controller = 'App\Controllers\\' . $controller;
        }

        $dispatch = new $controller($controllerName, $action);

        try {
            if (!method_exists($controller, $action)) {
                throw new \Exception("That method doesn't exist in the controller {$controllerName}");
            }

            call_user_func_array([$dispatch, $action], $url);
        } catch (\Exception $e) {
            user_error($e->getMessage());
        }
    }

    public static function redirect($location)
    {
        if (!headers_sent()) {
            header("Location: {$location}");
            exit();
        } else {
            echo '<script type="text/javascript">';
            echo 'window.location.href="' . $location . '";';
            echo '</script>';
            echo '<noscript>';
            echo '<meta http-equiv="refresh" content="0;url=' . $location . '" />';
            echo '</noscript>';
            exit;
        }
    }

    public static function back()
    {
        self::redirect($_SERVER['HTTP_REFERER']);
    }

    public static function currentUrl(): string
    {
        $protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === false ? 'http' : 'https';
        $host = $_SERVER['HTTP_HOST'];
        $requestUrl = $_SERVER['REQUEST_URI'];

        return $protocol . '://' . $host . $requestUrl;
    }

    public static function queryStringExists (): bool
    {
       return !empty(self::getQueryString()) ? true : false;
    }

    public static function getQueryString(): string
    {
        return $_SERVER['QUERY_STRING'];
    }

    public static function getRequestPath(): string
    {
        try {
            $urlArray = parse_url($_SERVER['REQUEST_URI']);
            if (!array_key_exists('path', $urlArray)) {
                throw new \Exception("The path doesn't exist.");
            }

            return $urlArray['path'];
        } catch (\Exception $e) {
            user_error($e->getMessage());
        }
    }
}
