<?php

namespace Core;

use Core\Application;
use Core\AuthorizationFactory;
use Core\View;
use Core\Input;
use Bootstrap\App;
use App\Traits\Redirectable;

class Controller extends Application
{
    use Redirectable;

    public $app;

    public $request;

    public $view;

    protected $controller;

    protected $action;

    protected $redirectToLoginRoute;

    protected $layout = 'default';

    public function __construct($controller, $action)
    {
        parent::__construct();
        $this->controller = $controller;
        $this->action = $action;
        $this->request = new Input();
        $this->view = new View();
        $this->view->setLayout($this->layout);
        $this->app = App::getInstance()->initializes();
        $this->onConstruct();
    }

    protected function render(string $viewPath, array $params = [])
    {
        $this->isUserLoggedIn() ? $this->view->render($viewPath, $params) : $this->redirect($this->redirectToLoginRoute);
    }

    protected function isUserLoggedIn()
    {
        return AuthorizationFactory::isLoggedIn();
    }

    public function onConstruct()
    {
        //
    }
}