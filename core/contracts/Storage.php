<?php

namespace Core\Contracts;

interface Storage
{
    public static function set(): bool;

    public static function get();

    public static function delete(): bool;

}
