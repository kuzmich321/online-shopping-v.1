<?php

use Core\Router;

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));

require_once(ROOT . DS . 'app' . DS . 'helpers' . DS . 'helpers.php');

require 'vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

spl_autoload_register(function ($className) {
    $classArray = explode('\\', $className);
    $class = array_pop($classArray);
    $subPath = strtolower(implode(DS, $classArray));
    $path = ROOT . DS . $subPath . DS . $class . '.php';
    if (file_exists($path)) {
        require_once($path);
    }
});

session_start();

$url = explode('/', trim(parse_url($_SERVER['REQUEST_URI'])['path'], '/')) ?? [];

$params = $_SERVER['QUERY_STRING'] ?? [];

Router::route($url, $params);