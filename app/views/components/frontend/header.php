<?php
    use App\Models\Categories;

    $categories = Categories::query('SELECT * FROM categories WHERE parent_id IS NULL')->results();
?>

<!-- header fixed -->
<div class="wrap_header fixed-header2 trans-0-4">
    <!-- Logo -->
    <a href="<?= route('home') ?>" class="logo">
        <img src="/static/images/icons/logo.png" alt="IMG-LOGO" class="logo">
    </a>

    <!-- Menu -->
    <div class="wrap_menu">
        <nav class="menu">
            <ul class="main_menu">
                <li>
                    <a href="<?= route('home') ?>">Домашняя страница</a>
                </li>

                <li>
                    <a href="product.html">Категории</a>
                    <ul class="sub_menu">
                        <?php foreach ($categories as $category): ?>
                            <li><a href="<?= route('products', ['cat' => [$category->id]]) ?>"><?= $category->name ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>

                <li class="sale-noti">
                    <a href="product.html">Скидки</a>
                </li>

                <li>
                    <a href="cart.html">Особенности</a>
                </li>

                <li>
                    <a href="blog.html">Наш блог</a>
                </li>

                <li>
                    <a href="about.html">О нас</a>
                </li>

                <li>
                    <a href="contact.html">Контакты</a>
                </li>
            </ul>
        </nav>
    </div>

    <!-- Header Icon -->
    <div class="header-icons">
        <a href="#" class="header-wrapicon1 dis-block">
            <i class="fa fa-user fa-2x header-icon1"></i>
        </a>

        <span class="linedivide1"></span>

        <div class="header-wrapicon2">
            <a href="<?= route('checkout') ?>" class="header-wrapicon1 dis-block">
                <i class="fa fa-shopping-bag fa-2x header-icon1 js-show-header-dropdown"></i>
            </a>
        </div>
    </div>
</div>

<!-- Header -->
<header class="header2">
    <!-- Header desktop -->
    <div class="container-menu-header-v2 p-t-26">
        <div class="topbar2">

            <!-- Logo2 -->
            <a href="<?= route('home') ?>" class="logo2">
                <img src="/static/images/icons/logo.png" alt="IMG-LOGO">
            </a>

            <div class="topbar-child2">
					<span class="topbar-email">
						fashe@example.com
					</span>

                <!--  -->
                <a href="#" class="header-wrapicon1 dis-block m-l-30">
                    <i class="fa fa-user fa-2x header-icon1"></i>
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2 m-r-13">
                    <a href="<?= route('checkout') ?>" class="header-wrapicon1 dis-block">
                        <i class="fa fa-shopping-bag fa-2x header-icon1 js-show-header-dropdown"></i>
                    </a>
                </div>
            </div>
        </div>

        <div class="wrap_header">

            <!-- Menu -->
            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li>
                            <a href="<?= route('home') ?>">Домашняя страница</a>
                        </li>

                        <li>
                            <a href="product.html">Категории</a>
                            <ul class="sub_menu">
                                <?php foreach ($categories as $category): ?>
                                    <li><a href="<?= route('products', ['cat' => [$category->id]]) ?>"><?= $category->name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>

                        <li class="sale-noti">
                            <a href="product.html">Скидки</a>
                        </li>

                        <li>
                            <a href="cart.html">Особенности</a>
                        </li>

                        <li>
                            <a href="blog.html">Наш блог</a>
                        </li>

                        <li>
                            <a href="about.html">О нас</a>
                        </li>

                        <li>
                            <a href="contact.html">Контакты</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="<?= route('home') ?>" class="logo-mobile">
            <img src="static/images/icons/logo.png" alt="IMG-LOGO">
        </a>

        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">
                <a href="#" class="header-wrapicon1 dis-block">
                    <i class="fa fa-user fa-2x header-icon1"></i>
                </a>

                <span class="linedivide2"></span>

                <div class="header-wrapicon2">
                    <a href="<?= route('checkout') ?>" class="header-wrapicon1 dis-block">
                        <i class="fa fa-shopping-bag fa-2x header-icon1"></i>
                    </a>
                </div>
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu">
        <nav class="side-menu">
            <ul class="main-menu">

                <li class="item-menu-mobile">
                    <a href="<?= route('home') ?>">Домашняя страница</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="product.html">Категории</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="product.html">Скидки</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="cart.html">Особенности</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="blog.html">Наш блог</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="about.html">О нас</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="contact.html">Контакты</a>
                </li>
            </ul>
        </nav>
    </div>
</header>