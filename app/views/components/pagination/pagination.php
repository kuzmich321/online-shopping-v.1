<?php
    $currentPage = $this->pages->getCurrentPage();
    $totalPages = $this->pages->getTotalPages();
?>
<ul class="pagination m-auto">
    <li><a class="page-link" href="<?= $this->pages->getLinkRoute(1) ?>">First</a></li>
    <li class="paginate_button page-item <?php if($currentPage <= 1){ echo 'disabled'; } ?>">
        <a class="page-link" href="<?php if($currentPage <= 1){ echo '#'; } else { echo $this->pages->getLinkRoute($currentPage - 1); } ?>">Prev</a>
    </li>
    <li class="paginate_button page-item <?php if($currentPage >= $totalPages){ echo 'disabled'; } ?>">
        <a class="page-link" href="<?php if($currentPage >= $totalPages){ echo '#'; } else { echo $this->pages->getLinkRoute($currentPage + 1); } ?>">Next</a>
    </li>
    <li class="paginate_button page-item"><a class="page-link" href="<?php echo $this->pages->getLinkRoute($totalPages); ?>">Last</a></li>
</ul>