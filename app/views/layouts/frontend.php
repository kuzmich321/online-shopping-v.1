<?php
    use App\Assets\BaseAssets;
    use App\Assets\FrontendAssets;

    $baseAssets = new BaseAssets();
    $frontendAssets = new FrontendAssets();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?= $this->content('head') ?>

    <script src="https://kit.fontawesome.com/0f42d6b70d.js" crossorigin="anonymous"></script>

    <link rel="icon" type="image/png" href="static/images/icons/favicon.png">

    <?= $frontendAssets->includeCss() ?>

    <title><?= $this->getSiteTitle() ?></title>
</head>
<body>

<body class="animsition">

<?= $this->component('frontend/header') ?>

<?= $this->content('body') ?>

<?= $this->component('frontend/footer') ?>

<?= $this->component('frontend/btn-back-to-top') ?>

<!--============ scripts =============-->
<?= $baseAssets->includeJs() ?>
<?= $frontendAssets->includeJs() ?>
<?= $this->content('scripts') ?>

</body>
</html>
