<?php
    use App\Assets\BaseAssets;
    use App\Assets\AdminAssets;

    $baseAssets = new BaseAssets();
    $adminAssets = new AdminAssets();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <?= $this->content('head'); ?>

    <script src="https://kit.fontawesome.com/0f42d6b70d.js" crossorigin="anonymous"></script>

    <link rel="shortcut icon" href="#">

    <?= $adminAssets->includeCss() ?>

    <title><?= $this->getSiteTitle(); ?></title>
</head>
<body id="page-top">

<div id="wrapper">

    <?php $this->component('admin/sidebar'); ?>

    <div id="content-wrapper" class="d-flex flex-column">

        <div id="content">

            <?php $this->component('admin/topbar'); ?>

            <div class="container-fluid">

                <?= $this->content('body'); ?>

            </div>


        </div>

        <?php $this->component('admin/footer'); ?>

    </div>

</div>

<?php $this->component('admin/scroll-top-btn'); ?>

<?php $this->component('admin/logout-modal'); ?>


<?= $baseAssets->includeJs() ?>
<?= $adminAssets->includeJs() ?>

<?= $this->content('scripts'); ?>

</body>
</html>
