<?php $this->setSiteTitle('Products'); ?>

<?php $this->start('head'); ?>

<link rel="stylesheet" href="/node_modules/bstreeview/src/css/bstreeview.css">

<?php $this->end(); ?>


<?php $this->start('body'); ?>

<div class="row">
    <div class="col-md-2 pt-5">
        <div id="tree">

        </div>
    </div>
    <div class="col-md-10 pt-5">

        <?php foreach (array_chunk($this->products, 4) as $block): ?>
            <div class="row">
        <?php foreach ($block as $product): ?>
            <div class="col-sm d-flex flex-column justify-content-center align-items-center">
                <div class="card mb-3" style="width: 18rem">
                    <img src="<?php echo "/static/products/{$product->category_id}/{$product->id}/{$product->id}.jpeg"; ?>" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title text-dark text-center"><?= $product->name ?></h5>
                        <p class="card-text text-primary text-center"><?= $product->price ?> ₽</p>
                        <div class="btn-group btn-group-sm">
                            <a class="btn btn-primary m-1" href="<?= route('admin.products.show', $product->id) ?>" role="button"><small>Смотреть</small></a>
                            <a class="btn btn-warning m-1" href="<?= route('admin.products.edit', $product->id) ?>" role="button"><small>Редактировать</small></a>
                            <a class="btn btn-danger m-1" href="<?= route('admin.products.delete', $product->id) ?>" role="button"><small>Удалить</small></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
            </div>
        <?php endforeach; ?>

        <?php if($this->pages && $this->pages->getTotalPages() > 1): ?>
        <div class="row d-flex align-items-center justify-content-center m-3">
            <?= $this->component('pagination/pagination', ['pages' => $this->pages]); ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php $this->end(); ?>


<?php $this->start('scripts'); ?>

<script src="/node_modules/bstreeview/src/js/bstreeview.js"></script>

<script>
    let tree = <?= $this->categories; ?>;

    $('#tree').bstreeview({
        data: tree,
        openNodeLinkOnNewTab: false
    });
</script>

<?php $this->end(); ?>
