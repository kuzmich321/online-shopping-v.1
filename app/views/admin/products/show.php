<?php $this->setSiteTitle('Страничка товара'); ?>

<?php $this->start('body'); ?>

<div class="container d-flex justify-content-center">
    <div class="card">
        <img src="/products/coats/<?= $this->product->id . DS . $this->product->id . '.jpeg' ?>" class="card-img-top" alt="...">
        <div class="card-body text-center">
            <h5 class="card-title text-dark"><?= $this->product->name ?></h5>
            <p class="card-text text-primary"><?= $this->product->price ?> руб.</p>
            <a href="<?= route('admin.products.edit', $this->product->id) ?>" class="btn btn-primary">Редактировать</a>
        </div>
    </div>
</div>

<?php $this->end(); ?>