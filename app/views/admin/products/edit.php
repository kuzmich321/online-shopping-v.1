<?php $this->setSiteTitle('Редактирование товара'); ?>

<?php $this->start('body'); ?>

<div class="card w-50 m-auto bg-transparent">
    <h4 class="card-header text-primary text-center">Редактировать</h4>
    <div class="card-body">
        <form class="mx-auto product" method="POST" action="<?= route('admin.products.update', $this->product->id) ?>">
            <div class="form-group">
                <input type="text" class="form-control form-control-product" id="name" name="name" placeholder="Product name"
                       required value="<?= $this->product->name ?>">
            </div>
            <div class="form-group row">
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-product" id="price" name="price"
                           placeholder="Цена" value="<?= $this->product->price ?>">
                </div>
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <select class="form-control" id="gender" name="gender">
                        <option value="<?= $this->genders['male'] ?>">Муж.</option>
                        <option value="<?= $this->genders['female'] ?>">Жен.</option>
                        <option value="<?= $this->genders['unisex'] ?>">Юнисекс</option>
                    </select>
                </div>
            </div>
            <div class="row justify-content-center">
                <button type="submit" class="btn btn-primary btn-product btn-block w-50">
                    Редактировать
                </button>
            </div>
            <hr>
        </form>
    </div>
</div>

<?php $this->end(); ?>
