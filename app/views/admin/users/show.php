<?php $this->start('body'); ?>

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <div class="row">
            <h1 class="display-4 mr-5"><?= $this->user->email ?></h1>
            <img src="/static/avatars/<?= $this->user->id ?>.png" alt="..." style="width: 50px; height: 50px;">
        </div>
        <p class="lead">Имя пользователя: <?= $this->user->username ?></p>
        <h4>Адрес доставки:</h4>
        <p class="lead"><?= $this->user->locality ?>, <?= $this->user->street ?> <?= $this->user->house_number ?>, кв. <?= $this->user->apartment_number ?></p>
        <p>Телефон: <?= $this->user->phone ?></p>
    </div>
</div>

<?php $this->end(); ?>
