<?php $this->start('body'); ?>


<div class="card w-50 m-auto bg-transparent">
    <h4 class="card-header text-primary text-center">Редактировать</h4>
    <div class="card-body">
        <form class="mx-auto user" method="POST" action="<?= route('admin.users.update', $this->user->id) ?>">
            <div class="form-group">
                <input type="email" class="form-control form-control-user" id="email" name="email" placeholder="Email"
                       required value="<?= $this->user->email ?>">
            </div>
            <div class="form-group row">
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="locality" name="locality"
                           placeholder="Населенный пункт" value="<?= $this->user->locality ?>">
                </div>
                <div class="col-sm-4 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="street" name="street"
                           placeholder="Улица" value="<?= $this->user->street ?>">
                </div>
                <div class="col-sm-2 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="house_number" name="house_number"
                           placeholder="Номер дома" value="<?= $this->user->house_number ?>">
                </div>
                <div class="col-sm-2 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" id="apartment_number" name="apartment_number"
                           placeholder="Квартира" value="<?= $this->user->apartment_number ?>">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" class="form-control form-control-user" id="password" name="password"
                           placeholder="Изменить пароль">
                </div>
                <div class="col-sm-6">
                    <input type="password" class="form-control form-control-user" id="confirm" name="confirm"
                           placeholder="Повторите пароль">
                </div>
            </div>
            <div class="row justify-content-center">
                <button type="submit" class="btn btn-primary btn-user btn-block w-50">
                    Редактировать
                </button>
            </div>
            <hr>
        </form>
    </div>
</div>

<?php $this->end(); ?>
