<?php $this->setSiteTitle('Users'); ?>

<?php $this->start('head'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.21/datatables.min.css"/>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.21/datatables.min.js"></script>

<?php $this->end(); ?>

<?php $this->start('body'); ?>
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Users</h6>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="dataTables_length" id="dataTable_length"><label>Show <select name="dataTable_length"
                                                                                                 aria-controls="dataTable"
                                                                                                 class="custom-select custom-select-sm form-control form-control-sm">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select> entries</label></div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div id="dataTable_filter" class="dataTables_filter"><label>Search:<input type="search"
                                                                                                  class="form-control form-control-sm"
                                                                                                  placeholder=""
                                                                                                  aria-controls="dataTable"></label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0"
                               role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                            <thead>
                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-sort="ascending" aria-label="Name: activate to sort column descending"
                                    style="width: 100px;">Аватар
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-label="Position: activate to sort column ascending" style="width: 402px;">
                                    Email
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-label="Office: activate to sort column ascending" style="width: 201px;">Дата
                                    регистрации
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-label="Age: activate to sort column ascending" style="width: 101px;">Смотреть
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-label="Age: activate to sort column ascending" style="width: 101px;">Редактировать
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"
                                    aria-label="Age: activate to sort column ascending" style="width: 101px;">Заблокировать
                                </th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th rowspan="1" colspan="1">Аватар</th>
                                <th rowspan="1" colspan="1">Email</th>
                                <th rowspan="1" colspan="1">Дата регистрации</th>
                                <th rowspan="1" colspan="1">Смотреть</th>
                                <th rowspan="1" colspan="1">Редактировать</th>
                                <th rowspan="1" colspan="1">Заблокировать</th>
                            </tr>
                            </tfoot>
                            <tbody>

                            <?php foreach ($this->users as $user): ?>
                                <tr role="row" class="odd">
                                    <td class="sorting_1 text-center">
                                        <?php if (file_exists(ROOT . "/static/avatars/{$user->id}.png")): ?>
                                            <img src="/static/avatars/<?= $user->id ?>.png" alt="" width="50px" height="50px">
                                        <?php else: ?>
                                            <i class="fas fa-user fa-2x"></i>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= $user->email ?></td>
                                    <td><?= date('d-m-Y H:m:s', strtotime($user->created_at)) ?></td>
                                    <td>
                                        <a href="<?= route('admin.users.show', $user->id) ?>" class="btn btn-primary m-1" role="button" aria-pressed="true">Смотреть</a>
                                    </td>
                                    <td>
                                        <a href="<?= route('admin.users.edit', $user->id) ?>" class="btn btn-warning m-1" role="button" aria-pressed="true">Редактировать</a>
                                    </td>
                                    <td>
                                        <a href="<?= route('admin.users.block', $user->id) ?>" class="btn btn-danger m-1
                                        <?php if($user->status === 2) echo 'disabled'; ?>"
                                           role="button"
                                           aria-pressed="true">Заблокировать</a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                            <?= $this->component('pagination/pagination', ['pages' => $this->pages]) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->end(); ?>

<?php $this->start('scripts'); ?>

<script src="/static/js/jquery.dataTables.min.js"></script>
<script src="/static/js/dataTables.bootstrap4.min.js"></script>

<script>
    $(document).ready(function () {
        $('#myTable').DataTable();
    });
</script>

<?php $this->end(); ?>
