<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        a {
            outline: none;
        }

        .container {
            width: 80%;
            margin: auto;
        }

        .jumbotron {
            padding: 2rem 1rem;
            margin-bottom: 2rem;
            background-color: #eaecf4;
            border-radius: 0.3rem;
        }

        @media (min-width: 576px) {
            .jumbotron {
                padding: 4rem 2rem;
            }
        }

        .display-4 {
            font-size: 3.5rem;
            font-weight: 300;
            line-height: 1.2;
        }

        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }

        .text-primary {
            color: #4e73df;
        }

        .my-4 {
            margin-top: 1.5rem;
        }

        .btn {
            display: inline-block;
            font-weight: 400;
            color: #858796;
            text-align: center;
            vertical-align: middle;
            background-color: transparent;
            border: 1px solid transparent;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.35rem;
            cursor: pointer;
        }

        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }

        .btn-lg > .btn {
            padding: 0.5rem 1rem;
            font-size: 1.25rem;
            line-height: 1.5;
            border-radius: 0.3rem;
        }

        .btn-primary {
            color: #fff;
            background-color: #4e73df;
            border-color: #4e73df;
        }
    </style>
    <title></title>
</head>
<body>
<div class="container">

    <div class="jumbotron">
        <h2 class="display-4">Добрый день!</h2>
        <p class="lead text-primary">Будьте добры подтвердить, что это ваш почтовый ящик.</p>
        <hr class="my-4">
        <p>Спасибо за пользование нашими услугами!</p>
        <p class="lead">
            <a href="{{ domain }}/register/confirmed?verification_key={{ verification_key }}" class="btn btn-primary btn-lg">Подтверждаю. Это мой почтовый ящик :)</a>
        </p>
    </div>

</div>
</body>
</html>
