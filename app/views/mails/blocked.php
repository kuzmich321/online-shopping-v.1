<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>

        .container {
            width: 80%;
            margin: auto;
        }

        .jumbotron {
            padding: 2rem 1rem;
            margin-bottom: 2rem;
            background-color: #eaecf4;
            border-radius: 0.3rem;
        }

        @media (min-width: 576px) {
            .jumbotron {
                padding: 4rem 2rem;
            }
        }

        .display-4 {
            font-size: 3.5rem;
            font-weight: 300;
            line-height: 1.2;
        }

        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }

        .text-primary {
            color: #4e73df;
        }

        .lead {
            font-size: 1.25rem;
            font-weight: 300;
        }
    </style>
    <title></title>
</head>
<body>
<div class="container">

    <div class="jumbotron">
        <h2 class="display-4">Добрый день.</h2>
        <p class="lead text-primary">Вы заблокированы</p>
    </div>

</div>
</body>
</html>
