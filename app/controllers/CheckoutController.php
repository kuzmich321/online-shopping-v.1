<?php

namespace App\Controllers;

use App\Controllers\FrontendController;
use App\Models\Orders;

class CheckoutController extends FrontendController
{
    public function index()
    {
        $orders = Orders::joinProductsByCurrentUserId();
        $totalPrice = 0;

        if ($this->request->isPost()) {
            $updatedOrders = $this->request->get('orders');
            $deletedOrder = $this->request->get('deletedOrder');

            if ($deletedOrder) {
                Orders::delete($deletedOrder);
            }

            if ($updatedOrders) {
                foreach ($orders as $order) {
                    try {
                        if (!array_key_exists($order->id, $updatedOrders)) {
                            throw new \Exception('Что-то пошло не так...');
                        }

                        if ($order->count != $updatedOrders[$order->id]) {
                            Orders::update(['count' => $updatedOrders[$order->id]], $order->id);
                        }
                    } catch (\Exception $e) {
                        user_error($e->getMessage());
                    }
                }
            }

            return $this->back();
        }

        foreach ($orders as $order) {
            $totalPrice += $order->price * $order->count;
        }

        $this->render('frontend/checkout/index', [
            'orders' => $orders,
            'totalPrice' => $totalPrice
        ]);
    }

    public function proceed()
    {
        //
    }
}
