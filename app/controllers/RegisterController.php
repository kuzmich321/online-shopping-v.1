<?php

namespace App\Controllers;

use Core\Controller;
use Core\Cookie;
use Core\Session;
use Core\AuthorizationFactory;
use App\Models\Users;

class RegisterController extends Controller
{
    public function onConstruct()
    {
        AuthorizationFactory::authorize();
    }

    public function index()
    {
       AuthorizationFactory::isLoggedIn() ? $this->redirect('/') : $this->view->render('register/index');
    }

    public function register()
    {
        $newUser = new Users();
        if ($this->request->isPost() && ($this->request->get('password') === $this->request->get('confirm'))) {

            $fields = [
                'username' => $this->request->get('username'),
                'email' => $this->request->get('email'),
                'password' => password_hash($this->request->get('password'), PASSWORD_DEFAULT),
                'status' => Users::STATUS_AWAITING_ACTIVIATION,
                'verification_key' => md5(time() . $fields['username'])
            ];

            $newUser->insert($fields)->createProfile();
            $this->sendConfirmationEmail($fields['email'], $fields['verification_key']);

            $this->redirect('/');
        }
    }

    private function sendConfirmationEmail(string $email, string $vkey)
    {
        $mail = $this->app->getAlias('mailer');
        $mail->Subject = 'Confirm Email';
        $mail->addAddress($email, 'Dear User');

        $path = $this->app->getAlias('@views') . DS . 'mails' . DS . 'confirmation.php';
        $message = replaceStringsInFile($path, [
            '{{ domain }}' => getenv('DOMAIN'),
            '{{ verification_key }}' => $vkey
        ]);

        $mail->msgHTML($message);
        $mail->isHTML(true);
        $mail->send();
    }

    public function confirmed()
    {
        if ($this->request->get('verification_key')) {
            $vkey = $this->request->get('verification_key');
            (new Users())->query("UPDATE users SET status = ?, code_activated = now(), verification_key = ? WHERE verification_key = '{$vkey}';", [Users::STATUS_ACTIVATED, null]);

            $this->redirect('/login');
        }
    }
}