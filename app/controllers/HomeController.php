<?php

namespace App\Controllers;

use App\Controllers\FrontendController;

class HomeController extends FrontendController
{
    public function index()
    {
        $this->view->render('frontend/index');
    }
}
