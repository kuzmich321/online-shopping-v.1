<?php

namespace App\Controllers;

use App\Controllers\FrontendController;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Categories;
use Core\Database\Database;
use Core\Paginator;

class ProductsController extends FrontendController
{
    public function index()
    {
        $categories = Categories::buildMenu('products');
        $products = [];
        $pages = null;

        $currentCategory = null;

        if ($currentCategory = $this->request->get('cat')) {
            $children = Categories::getChildrenIdString($currentCategory);
            $where = "WHERE category_id IN ({$children}) ";
            // ToDo more filters
            if ($gender = $this->request->get('gender')) {
                $where .= "AND gender = {$gender}";
            }

            $pages = new Paginator(Products::query("SELECT COUNT (*) FROM products {$where}")->first()->count);
            $products = Database::connection('sphinx')->query("SELECT * FROM products {$where} LIMIT {$pages->offset}, {$pages->perPage}")->results();
        }

        $this->view->render('frontend/products/index', [
            'products' => $products,
            'pages' => $pages,
            'categories' => json_encode($categories),
            'currentCategory' => $currentCategory
        ]);
    }

    public function show($id)
    {
        $this->view->render('frontend/products/show', [
            'product' => Products::findById($id)
        ]);
    }

    public function addToBag($id)
    {
        if ($this->isUserLoggedIn()) {
            $currentUser = currentUser();
            $product = Products::findById($id);
            $existingOrder = Orders::query("SELECT * FROM orders WHERE user_id = ? AND product_id = ?", [$currentUser->id, $product->id])->first() ?? null;

            if ($existingOrder && $existingOrder->product_id === $product->id) {
                Orders::update(['count' => $existingOrder->count + $this->request->get('count')], $existingOrder->id);
            } else {
                (new Orders())->insert([
                    'user_id' => $currentUser->id,
                    'profile_id' => $currentUser->id,
                    'product_id' => $product->id,
                    'count' => $this->request->get('count')
                ]);
            }

            return $this->back();

        } else {
            $this->redirect('/login');
        }
    }
}