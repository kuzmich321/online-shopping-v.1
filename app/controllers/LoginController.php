<?php

namespace App\Controllers;

use Core\Controller;
use Core\Cookie;
use Core\Session;
use Core\AuthorizationFactory;
use App\Models\Users;

class LoginController extends Controller
{
    public function onConstruct()
    {
        AuthorizationFactory::authorize();
    }

    public function index()
    {
        AuthorizationFactory::isLoggedIn() ? $this->redirect('/') : $this->view->render('login/index');
    }

    public function login()
    {
        $user = new Users();

        if ($this->request->isPost()) {
            $user->assign($_POST);
            $user = $user->query("SELECT * FROM users WHERE email = ?;", [$user->email])->first();

            if ($user && password_verify($this->request->get('password'), $user->password) &&
                $user->status === Users::STATUS_ACTIVATED &&
                ($user->role === Users::ID_TYPE_USER || $user->role === Users::ID_TYPE_ADMINISTRATOR)) {

                Session::set('frontend_user_id', $user->id);

                if ($this->request->get('remember_me') === 'on') {
                    $secondsPerWeek = 604800;
                    Cookie::set('frontend_user_id', md5($user->id), $secondsPerWeek, '/');
                }

                $redirectUrl = getRedirectUrl('/');
                Session::delete('redirect_url');
                $this->redirect($redirectUrl);
            } else {
                $this->redirect('/login');
            }
        }
    }
}
