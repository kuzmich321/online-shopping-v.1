<?php

namespace App\Controllers;

use Core\AuthorizationFactory;
use Core\Controller;
use App\Models\Users;
use Core\Session;

class FrontendController extends Controller
{
    const accessRole = Users::ID_TYPE_USER;

    protected $layout = 'frontend';

    protected $redirectToLoginRoute = '/login';

    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
    }

    public function onConstruct()
    {
        AuthorizationFactory::authorize();
        if (AuthorizationFactory::isLoggedIn()) {
            AuthorizationFactory::verifyRole(self::accessRole);
        } else {
            Session::set('redirect_url', $_SERVER['REQUEST_URI']);
        }
    }
}
