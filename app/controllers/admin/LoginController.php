<?php

namespace App\Controllers\Admin;

use Core\Controller;
use Core\AuthorizationFactory;
use Core\Session;
use Core\Cookie;
use App\Models\Users;

class LoginController extends Controller
{
    public function onConstruct()
    {
        AuthorizationFactory::authorize('backend_user_id');
    }

    public function index()
    {
        AuthorizationFactory::isLoggedIn() ? $this->redirect('/admin/dashboard') : $this->view->render('login/index');
    }

    public function login()
    {
        $user = new Users();

        if ($this->request->isPost()) {
            $user->assign($_POST);
            $user = $user->query("SELECT * FROM users WHERE email = ?;", [$user->email])->first();

            if ($user && password_verify($this->request->get('password'), $user->password) &&
                $user->status === Users::STATUS_ACTIVATED &&
                ($user->role === Users::ID_TYPE_ADMINISTRATOR)) {

                Session::set('backend_user_id', $user->id);

                if ($this->request->get('remember_me') === 'on') {
                    $secondsPerWeek = 604800;
                    Cookie::set('backend_user_id', md5($user->id), $secondsPerWeek, '/admin');
                }

                $redirectUrl = getRedirectUrl('/admin/dashboard');
                Session::delete('redirect_url');
                $this->redirect($redirectUrl);
            } else {
                $this->redirect('/admin/login');
            }
        }
    }
}