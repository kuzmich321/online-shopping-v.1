<?php

namespace App\Controllers\Admin;

use Core\AuthorizationFactory;
use Core\Controller;
use App\Models\Users;
use Core\Session;

class AdminController extends Controller
{
    const accessRole = Users::ID_TYPE_ADMINISTRATOR;

    protected $layout = 'admin';

    protected $redirectToLoginRoute = '/admin/login';

    public function __construct($controller, $action)
    {
        parent::__construct($controller, $action);
    }

    public function onConstruct()
    {
        AuthorizationFactory::authorize('backend_user_id');
        if (AuthorizationFactory::isLoggedIn()) {
            AuthorizationFactory::verifyRole(self::accessRole);
        } else {
            Session::set('redirect_url', $_SERVER['REQUEST_URI']);
        }
    }
}
