<?php

namespace App\Controllers\Admin;

use Core\Paginator;
use App\Controllers\Admin\AdminController;
use App\Models\Users;
use App\Models\Profiles;

class UsersController extends AdminController
{
    public function index()
    {
        $pages = new Paginator(Users::getTotalCount());

        $this->render('admin/users/index', [
            'users' => Users::paginate($pages->perPage, $pages->offset),
            'pages' => $pages
        ]);
    }

    public function show($id)
    {
        $this->render('admin/users/show', [
            'user' => Users::query("SELECT * FROM users FULL JOIN profiles ON users.id = profiles.id WHERE users.id = ?", [$id])->first()
        ]);
    }

    public function edit($id)
    {
        $this->render('admin/users/edit', [
            'user' => Users::query("SELECT * FROM users FULL JOIN profiles ON users.id = profiles.id WHERE users.id = ?", [$id])->first()
        ]);
    }

    public function update($id)
    {
        if ($this->request->isPost()) {

            $userFields = [
                'email' => $this->request->get('email')
            ];

            $profilesFields = [
                'locality' => $this->request->get('locality'),
                'street' => $this->request->get('street'),
                'house_number' => $this->request->get('house_number'),
                'apartment_number' => $this->request->get('apartment_number')
            ];

            if ($this->request->get('password') === '') {
                unset($_POST['password'], $_POST['confirm']);
            } else {
                if ($this->request->get('password') === $this->request->get('confirm')) {
                    $userFields['password'] = password_hash($this->request->get('password'), PASSWORD_DEFAULT);
                }
            }

            Users::update($userFields, $id);
            Profiles::update($profilesFields, $id);

            $this->redirect("/admin/users/show/{$id}");
        }
    }

    public function block($id)
    {
        Users::query('UPDATE users SET status = ' . Users::STATUS_BLOCKED . ' WHERE id = ?', [$id]);

        $mail = $this->app->getAlias('mailer');

        $mail->Subject = 'You have been blocked';
        $mail->addAddress(Users::findById($id)->email, 'Dear User');
        $mail->msgHTML(file_get_contents($this->app->getAlias('@views') . DS . 'mails' . DS . 'blocked.php'));
        $mail->isHTML(true);

        $mail->send();

        $this->back();
    }
}
