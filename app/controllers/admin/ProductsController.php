<?php

namespace App\Controllers\Admin;

use App\Controllers\Admin\AdminController;
use App\Models\Categories;
use App\Models\Products;
use Core\Paginator;

class ProductsController extends AdminController
{
    public function index()
    {
        $categories = Categories::buildMenu();

        $products = [];
        $pages = null;

        if ($this->request->get('cat')) {
            $children = Categories::getChildrenIdString($this->request->get('cat'));
            $pages = new Paginator(Products::query("SELECT COUNT (*) FROM products WHERE category_id IN ({$children})")->first()->count);
            $products = Products::query("SELECT * FROM products WHERE category_id IN ({$children}) LIMIT {$pages->perPage} OFFSET {$pages->offset}")->results();
        }

        $this->render('admin/products/index', [
            'categories' => json_encode($categories),
            'products' => $products,
            'pages' => $pages
        ]);
    }

    public function show($id)
    {
        $this->render('admin/products/show', [
           'product' => Products::findById($id)
        ]);
    }

    public function edit($id)
    {
        $this->render('admin/products/edit', [
           'product' => Products::findById($id),
            'genders' => Products::getGenders()
        ]);
    }

    public function update($id)
    {
        if ($this->request->isPost()) {
            $fields = [
              'name' => $this->request->get('name'),
              'price' => $this->request->get('price'),
              'gender' => $this->request->get('gender')
            ];

            Products::update($fields, $id);

            $this->redirect("/admin/products/show/{$id}");
        }
    }

    public function create()
    {
        $this->render('admin/products/create', [
            'genders' => Products::getGenders(),
            'categories' => Categories::query('SELECT * FROM categories WHERE parent_id IS NOT NULL')->results()
        ]);
    }

    public function store()
    {
        if ($this->request->isPost()) {
            $fields = [
                'name' => $this->request->get('name'),
                'price' => $this->request->get('price'),
                'gender' => $this->request->get('gender'),
                'category_id' => $this->request->get('category_id')
            ];

            (new Products())->insert($fields);

            $this->redirect("/admin/products/index");
        }
    }

    public function delete($id)
    {
        Products::delete($id);

        $this->redirect('/admin/products/index');
    }

    public function destroy($id)
    {
        //
    }
}