<?php

namespace App\Controllers\Admin;

use App\Controllers\Admin\AdminController;

class DashboardController extends AdminController
{
    public function index()
    {
        $this->render('admin/home/index');
    }
}