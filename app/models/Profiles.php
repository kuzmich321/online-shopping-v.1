<?php

namespace App\Models;

use Core\Database\Model;

class Profiles extends Model
{
    protected static $table = 'profiles';

    public $user_id;

    public $phone;

    public $locality;

    public $street;

    public $apartment_number;

    public $house_number;
}
