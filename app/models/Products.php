<?php

namespace App\Models;

use Core\Database\Model;

class Products extends Model
{
    protected static $table = 'products';

    const ID_GENDER_MALE = 1;
    const ID_GENDER_FEMALE = 2;
    const ID_GENDER_UNISEX = 3;

    public $id;

    public $name;

    public $gender;

    public $price;

    public $category_id;

    public $deleted;

    public $created_at;

    public static function getGenders()
    {
        return [
          'male' => self::ID_GENDER_MALE,
          'female' => self::ID_GENDER_FEMALE,
          'unisex' => self::ID_GENDER_UNISEX
        ];
    }
}
