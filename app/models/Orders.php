<?php

namespace App\Models;

use Core\Database\Model;
use App\Models\OrderItem;

class Orders extends Model
{
    protected static $table = 'orders';

    const ID_STATUS_IN_CHECKOUT = 0;
    const ID_STATUS_PAYED = 1;
    const ID_STATUS_SHIPPED = 2;

    public $id;

    public $user_id;

    public $profile_id;

    public $product_id;

    public $created_at;

    public $updated_at;

    public $status;

    public $count;

    public $amount;

    public static function joinProductsByCurrentUserId()
    {
        return self::query("SELECT o.id as id, o.user_id, o.product_id, o.created_at as  order_created_at, 
                    o.updated_at as order_updated_at, o.status, o.count, p.name, p.gender, p.price, p.category_id, p.deleted as product_deleted, 
                    p.created_at as product_created_at
                    FROM orders as o RIGHT JOIN products as p on o.product_id = p.id WHERE user_id = ?", [currentUser()->id])->results();
    }
}
