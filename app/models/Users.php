<?php

namespace App\Models;

use Core\Database\Model;
use App\Models\Profiles;

class Users extends Model
{
    protected static $table = 'users';

    const ID_TYPE_USER = 1;
    const ID_TYPE_ADMINISTRATOR = 2;

    const STATUS_AWAITING_ACTIVIATION = 0;
    const STATUS_ACTIVATED = 1;
    const STATUS_BLOCKED = 2;

    public $id;

    public $username;

    public $email;

    public $first_name;

    public $second_name;

    public $patronymic;

    public $password;

    public $status;

    public $role;

    public $code_activated;

    public $created_at;

    public $verification_key;

    public function createProfile()
    {
        (new Profiles())->insert([
            'id' => self::query('SELECT id FROM users WHERE username = ?', [$this->username])->first()->id
        ]);
    }
}
