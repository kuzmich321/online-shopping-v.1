<?php

namespace App\Models;

use Core\Database\Model;

class Categories extends Model
{
    protected static $table = 'categories';

    public $id;

    public $name;

    public $parentId;

    public static function buildMenu(string $routeName = 'admin.products', string $attrNameForChildren = 'nodes')
    {
        $categories = self::query('SELECT id, name as text, parent_id FROM categories ORDER BY id')->results();

        foreach ($categories as $category) {
            $category->href = route($routeName, ['cat' => [$category->id]]);
        }

        return buildTreeFromObjects($categories, $attrNameForChildren);
    }

    public static function getChildren($id)
    {
        $children = self::query('SELECT id FROM categories WHERE id IN (SELECT child FROM categories_closure WHERE parent = :id)', $id);

        return $children->getCount() > 0 ? $children->results() : [];
    }

    public static function getChildrenIdString($id)
    {
        $children = self::getChildren($id);

        if (!empty($children)) {
            $result = [];

            foreach ($children as $child) {
                array_push($result, $child->id);
            }

            return implode(',', $result);
        }
    }
}
