<?php

namespace App\Assets;

use App\Assets\BaseAssets;

class FrontendAssets extends BaseAssets
{
    public array $js = [
        'main.js',
        'slick-custom.js',
        'cdn' => [
            'https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/js/animsition.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js',
            'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.2/js/lightbox.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js',
        ]
    ];

    public array $css = [
        'util.css',
        'main.css',
        'cdn' => [
            'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.0/animate.compat.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/animsition/4.0.2/css/animsition.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.2/css/lightbox.min.css'
        ]
    ];
}
