<?php

namespace App\Assets;

use App\Assets\BaseAssets;

class AdminAssets extends BaseAssets
{
    public array $js = [
      'bootstrap.bundle.min.js',
      'jquery.easing.min.js',
      'sb-admin-2.min.js',
    ];

    public array $css = [
        'sb-admin-2.min.css',
        'cdn' => [
            'https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'
        ]
    ];
}
