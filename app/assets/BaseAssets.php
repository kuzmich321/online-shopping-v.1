<?php

namespace App\Assets;

use Core\Assets\Asset;

class BaseAssets extends Asset
{
    protected string $rootAssets = '/static';

    public array $js = [
        'jquery.min.js'
    ];
}
