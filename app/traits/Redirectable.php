<?php

namespace App\Traits;

use Core\Router;

trait Redirectable
{
    public function redirect(string $path = '/')
    {
        Router::redirect($path);
    }

    public function back()
    {
        Router::back();
    }
}