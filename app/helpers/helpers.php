<?php

use Core\Session;
use Core\Router;
use Core\AuthorizationFactory;

function dd($data)
{
    echo '<pre>';
    highlight_string("<?php\n " . var_export($data, true) . "?>");
    echo '</pre>';
    die();
}

function currentUser()
{
    return AuthorizationFactory::getCurrentUser();
}

function route(string $routeName, $params = null)
{
    $route = trim(str_replace('.', '/', $routeName));

    if ($params) {
        if (is_array($params)) {
            $route .= '/?' . urldecode(http_build_query($params));
        } else {
            $route .= "/{$params}";
        }
    }

    return "/{$route}";
}

function currentUrl(): string
{
    return Router::currentUrl();
}

function queryStringExists(): bool
{
    return Router::queryStringExists();
}

function getQueryString(): string
{
    return Router::getQueryString();
}

function getRequestPath(): string
{
    return Router::getRequestPath();
}

function sanitize($dirty)
{
    return addslashes(htmlspecialchars($dirty));
}

function replaceStringsInFile(string $path, array $stringsToBeReplaced = []): string
{
    $file = file_get_contents($path);

    foreach ($stringsToBeReplaced as $key => $value) {
        $file = str_replace($key, $value, $file);
    }

    return $file;
}

function getRedirectUrl(string $defaultUrl): string
{
    $url = $defaultUrl;

    if ($redirectUrl = Session::get('redirect_url')) {
        $url = $redirectUrl;
    }

    return $url;
}

function startTracking()
{
    xhprof_enable(XHPROF_FLAGS_NO_BUILTINS | XHPROF_FLAGS_CPU | XHPROF_FLAGS_MEMORY);
}

function stopTracking()
{
    $xhprofData = xhprof_disable();
    $filePath = getenv('XHPROF_LOG_PATH');
    file_put_contents("{$filePath}report.txt", serialize($xhprofData));
    include_once getenv('XHPROF_PATH') . 'xhprof_lib/utils/xhprof_lib.php';
    include_once getenv('XHPROF_PATH') . '/xhprof_lib/utils/xhprof_runs.php';
    $xhprofData = file_get_contents("{$filePath}report.txt");
    $xhprofData = unserialize($xhprofData);
    $xhprof_runs = new XHProfRuns_Default();
    $run_id = $xhprof_runs->save_run($xhprofData, 'test');
}

// id and parent_id are required
function buildTreeFromObjects(array $items = [], string $attrNameForChildren = 'children')
{
    $children = [];

    foreach ($items as $item) {
        $children[$item->parent_id ?? 0][] = $item;
    }

    foreach ($items as $item) {
        if (isset($children[$item->id])) {
            $item->{$attrNameForChildren} = $children[$item->id];
        }
    }

    return $children[0] ?? [];
}