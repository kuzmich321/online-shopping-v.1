<?php

return [
  'sphinx' => [
      'driver' => getenv('DB_SPHINX_DRIVER'),
      'host' => getenv('DB_SPHINX_HOST'),
      'database' => '',
      'username' => '',
      'password' => ''
  ]
];
