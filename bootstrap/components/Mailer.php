<?php

namespace bootstrap\components;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Mailer extends PHPMailer
{
    public function __construct()
    {
        parent::__construct(true);
    }
}
