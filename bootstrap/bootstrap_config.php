<?php

define('@app', ROOT . DS . 'app');

return [
    'components' => [
        'mailer' => [
            'class' => 'bootstrap\components\Mailer',
            'SMTPDebug' => 0,
            'Mailer' => 'smtp',
            'Host' => getenv('MAIL_HOST'),
            'SMTPAuth' => true,
            'Username' => getenv('MAIL_USERNAME'),
            'Password' => getenv('MAIL_PASSWORD'),
            'SMTPSecure' => 'tls',
            'Port' => getenv('MAIL_PORT'),
            'From' => getenv('MAIL_FROM'),
            'FromName' => getenv('MAIL_FROM_NAME'),
            'CharSet' => 'UTF-8'
        ],
    ],

    'paths' => [
        '@views' => @app . DS . 'views',
        '@controllers' => @app . DS . 'controllers',
        '@models' => @app . DS . 'models'
    ]
];