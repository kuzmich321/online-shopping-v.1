<?php

namespace Bootstrap;

class App
{
    private static $instance = null;

    public $aliases = [];

    public static function getInstance(): App
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }
    
    public function initializes(): App
    {
        $result = require_once('bootstrap_config.php');

        foreach ($result['components'] as $alias => $component) {
            $classInstance = $component['class'];
            $class = new $classInstance;
            unset($component['class']);
            foreach ($component as $param => $value) {
                $class->$param = $value;
            }
            $this->aliases[$alias] = $class;
        }

        foreach ($result['paths'] as $key => $value) {
            $this->aliases[$key] = $value;
        }

        return $this;
    }

    public function getAlias($alias)
    {
        return $this->aliases[$alias] ?? null;
    }
}

